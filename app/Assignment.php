<?php

namespace App;
use App\Student;
use App\Degree;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = [
      'alumno_id',
      'grado_id',
      'seccion'
    ];

    public function students(){
    	return $this->belongsTo(Student::class);
    }

    public function degrees(){
    	return $this->belongsTo(Degree::class);
    }
}

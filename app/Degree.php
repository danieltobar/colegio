<?php

namespace App;
use App\Teacher;
use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{

	protected $fillable = [
      'nombre',
      'profesor_id'
    ];

    public function teachers(){
    	return $this->belongsTo(Teacher::class);
    }


}

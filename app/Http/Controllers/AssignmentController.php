<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Student;
use App\Degree;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$alumnosg = Assignment::all();

        $alumnosg = DB::table('assignments')->join('degrees', 'id', '=', 'grado_id')->join('students', 'id', '=', 'alumno_id')->select('assignment.*', 'students.nombre', 'degress.nombre')->get();

        return view('indexAlumnosG', compact('alumnosg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estudiantes = Student::all();
        $grados = Degree::all();  
        return view('createStudentDegree', compact('estudiantes', 'grados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Sgrado = new Assignment;
        $Sgrado->alumno_id = $request->get('estudiante');
        $Sgrado->grado_id = $request->get('grado');
        $Sgrado->seccion = $request->get('seccion');
        $Sgrado->save();

        return redirect()->route('assignment.index'); 
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $alumnosg = Assignment::find($id);
         $estudiantes = Student::all();
        $grados = Degree::all();
        return view('editAssignment', compact('alumnosg', 'estudiantes', 'grados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assignment = Assignment::find($id);
        $assignment->update($request->all());
        return redirect()->route('assignment.index');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $de = Assignment::destroy($id);
        return redirect()->route('assignment.index');
    }
}

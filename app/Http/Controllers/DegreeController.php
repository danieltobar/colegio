<?php

namespace App\Http\Controllers;

use App\Degree;
use App\Teacher;
use App\Student;
use Illuminate\Http\Request;

class DegreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $grados = Degree::all();  
        return view('indexGrado', compact('grados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $maestros = Teacher::all();  
        return view('createDegree', compact('maestros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $grado = new Degree;
        $grado->nombre = $request->get('nombre');
        $grado->profesor_id = $request->get('maestro');
        $grado->save();

        return redirect()->route('degree.index'); 
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $degree = Degree::find($id);
        $maestros = Teacher::all();

        $nombre_maestro = Teacher::find($degree->profesor_id);  
        return view('editDegree', compact('degree', 'maestros', 'nombre_maestro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $degree = Degree::find($id);
        $degree->update($request->all());
        return redirect()->route('degree.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Degree  $degree
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $de = Degree::destroy($id);
        return redirect()->route('degree.index');
    }
}

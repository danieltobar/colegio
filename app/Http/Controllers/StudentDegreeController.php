<?php

namespace App\Http\Controllers;
use App\Student;
use App\Degree;
use App\Student_Degree;
use Illuminate\Http\Request;

class StudentDegreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnosg = Student_Degree::all();  
        return view('indexAlumnosG', compact('alumnosg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $estudiantes = Student::all();
        $grados = Degree::all();  
        return view('createStudentDegree', compact('estudiantes', 'grados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Sgrado = new Student_Degree;
        $Sgrado->alumno_id = $request->get('estudiante');
        $Sgrado->grado_id = $request->get('grado');
        $Sgrado->seccion = $request->get('seccion');
        $Sgrado->save();

        return redirect()->route('student_degree.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Studen_Degree  $studen_Degree
     * @return \Illuminate\Http\Response
     */
    public function show(Studen_Degree $studen_Degree)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Studen_Degree  $studen_Degree
     * @return \Illuminate\Http\Response
     */
    public function edit(Studen_Degree $studen_Degree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Studen_Degree  $studen_Degree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Studen_Degree $studen_Degree)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Studen_Degree  $studen_Degree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Studen_Degree $studen_Degree)
    {
        //
    }
}

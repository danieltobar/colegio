<?php

namespace App;
use App\Studen_Degree;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

	 protected $fillable = [
      'nombre',
      'apellido',
      'genero',
      'fecha_nacimiento'

    ];
    public function degrees(){
    	return $this->hasMany(Studen_Degree::class);
    }

}

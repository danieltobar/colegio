<?php

namespace App;
use App\Degree;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
	protected $fillable = [
      'nombre',
      'apellido',
      'genero'
    ];

    public function degrees(){
    	return $this->hasMany(Degreee::class);
    }
}

<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>
	 <form method="post"" action="{{ route('degree.store') }}">
	    	@csrf
			<div>
		      
		     <div>
		        <label for="Name">Nombre:</label>
		        <input type="text" class="form-control" name="nombre">
		      </div>
		    </div>

		    <div>
				<div>
				    <label>maestros</label>
				    <select class="form-control" name="maestro">
				    	@foreach($maestros as $maestro)
				      		<option value="{{$maestro->id}}">{{ $maestro->nombre }} {{ $maestro->apellido }}</option>
				      	@endforeach
				    </select>
				</div>	
			</div> 

		    <div>
	          <div class="form-group col-md-6">
	            <button type="submit" class="btn btn-primary">Agregar</button>
	          </div>
	        </div>
	</form>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>
	 <form method="post"" action="{{ route('student.store') }}">
	    	@csrf
			<div>
		      
		     <div class="form-group col-md-6">
		        <label for="Name">Nombre:</label>
		        <input type="text" class="form-control" name="nombre">
		      </div>
		    </div>

			<div>
		    	<div class="form-group col-md-6">
		        	<label for="Name">Apellido:</label>
		        	<input type="text" class="form-control" name="apellido">
		      	</div>
		    </div>

		   <!--  <div class="form-group">
		    <label for="exampleFormControlSelect1">Example select</label>
		    <select class="form-control" id="exampleFormControlSelect1">
		      <option>Femenino</option>
		      <option>Masculino</option>
		     
		    </select>
		  </div> -->

		    <div>
		    	<div class="form-group col-md-6">
		        	<label for="Name">Genero:</label>
		        	<input type="text" class="form-control" name="genero">
		      	</div>
		    </div>

		    <div>
		    	<div class="form-group col-md-6">
		        	<label for="Name">Fecha Nacimiento:</label>
		        	<input type="text" class="form-control" name="fecha_nacimiento">
		      	</div>
		    </div>

		    <div>
	          <div class="form-group col-md-6">
	            <button type="submit" class="btn btn-primary">Agregar</button>
	          </div>
	        </div>
	</form>
</body>
</html>
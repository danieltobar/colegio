<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>
	 <form method="post"" action="{{ route('assignment.update', $alumnosg->id) }}">
	    	@csrf

	    	<input name="_method" type="hidden" value="PUT">
			<div>
		      
		     <div>
		        <label for="Name">Seccion:</label>
		        <input type="text" class="form-control" name="seccion">
		      </div>
		    </div>

		    <div>
				<div>
				    <label>Alumnos</label>
				    <select class="form-control" name="estudiante">
				    	@foreach($estudiantes as $estudiante)
				      		<option value="{{$estudiante->id}}">{{ $estudiante->nombre }} {{ $estudiante->apellido }}</option>
				      	@endforeach
				    </select>
				</div>	
			</div> 

			<div>
				<div>
				    <label>Grado</label>
				    <select class="form-control" name="grado">
				    	@foreach($grados as $grado)
				      		<option value="{{$grado->id}}">{{ $grado->nombre }} {{ $grado->apellido }}</option>
				      	@endforeach
				    </select>
				</div>	
			</div> 

		    <div>
	          <div class="form-group col-md-6">
	            <button type="submit" class="btn btn-primary">Agregar</button>
	          </div>
	        </div>
	</form>
</body>
</html>
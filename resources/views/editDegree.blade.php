<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>
	 <form method="post"" action="{{ route('degree.update', $degree->id) }}">
	    	@csrf
	    	<input name="_method" type="hidden" value="PUT">
			<div>
		      
		     <div class="form-group col-md-6">
		        <label for="Name">Nombre:</label>
		        <input type="text" class="form-control" name="nombre" value="{{$degree->nombre}}">
		      </div>
		    </div>

		     <div>
				<div>
				    <label>maestros</label>
				   
				    <select class="form-control" name="profesor_id">

				    	
						<option value="{{$degree->profesor_id}}">{{$nombre_maestro->nombre}} {{$nombre_maestro->apellido}}</option>

				    	@foreach($maestros as $maestro)
				      		<option value="{{$maestro->id}}">{{ $maestro->nombre }} {{ $maestro->apellido }}</option>

				      	@endforeach
				    </select>
				</div>	
			</div> 

		    <div>
	          <div>
	            <button type="submit" class="btn btn-primary">Editar</button>
	          </div>
	        </div>
	</form>
</body>
</html>
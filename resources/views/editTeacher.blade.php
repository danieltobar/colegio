<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>
	 <form method="post"" action="{{ route('teacher.update', $teacher->id) }}">
	    	@csrf
	    	<input name="_method" type="hidden" value="PUT">
			<div>
		      
		     <div class="form-group col-md-6">
		        <label for="Name">Nombre:</label>
		        <input type="text" class="form-control" name="nombre" value="{{$teacher->nombre}}">
		      </div>
		    </div>

			<div>
		    	<div class="form-group col-md-6">
		        	<label for="Name">Apellido:</label>
		        	<input type="text" class="form-control" name="apellido" value="{{$teacher->apellido}}">
		      	</div>
		    </div>

		    <div>
		    	<div class="form-group col-md-6">
		        	<label for="Name">Genero:</label>
		        	<input type="text" class="form-control" name="genero" value="{{$teacher->genero}}">
		      	</div>
		    </div>

		    <div>
	          <div class="form-group col-md-6">
	            <button type="submit" class="btn btn-primary">Editar</button>
	          </div>
	        </div>
	</form>
</body>
</html>
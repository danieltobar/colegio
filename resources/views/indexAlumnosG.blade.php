<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>

	<a href="http://colegio.test/">Inicio</a>
	<table>
	  <thead>
	    <tr>
	      <th>Alumno</th>
	      <th>Grado</th>
	      <th>Seccion</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($alumnosg as $al)
	    <tr>
	     
	      <td>{{$al->alumno_id}}</td>
	      <td>{{$al->grado_id}}</td>
	      <td>{{$al->seccion}} </td>
	     
	      <td><a href=" {{ route('assignment.edit', $al->id) }} ">Editar</a></td>
	     
	      <td>	
				<form method="POST" action="{{ route('assignment.destroy', $al->id) }}">
					@csrf
					{!! method_field('DELETE') !!}
					<button type="submit">Eliminar</button>
				</form>
	      	</td>



	    </tr>
	    @endforeach	
	  </tbody>
</table>

<a href=" {{ route('assignment.create') }} ">Crear Asignaciòn</a>

	
</body>
</html>
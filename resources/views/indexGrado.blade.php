<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>

	<a href="http://colegio.test/">Inicio</a>
	<table>
	  <thead>
	    <tr>
	      <th>Nombre</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($grados as $grado)
	    <tr>
	      <td>{{$grado->nombre}} </td>
	     
	      <td><a href=" {{ route('degree.edit', $grado->id) }} ">Editar</a></td>
	     
	      <td>	
				<form method="POST" action="{{ route('degree.destroy', $grado->id) }}">
					@csrf
					{!! method_field('DELETE') !!}
					<button type="submit">Eliminar</button>
				</form>
	      	</td>



	    </tr>
	    @endforeach	
	  </tbody>
</table>

<a href=" {{ route('degree.create') }} ">Crear Grado</a>

	
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>

	<a href="http://colegio.test/">Inicio</a>
	<table>
	  <thead>
	    <tr>
	      <th>Nombre</th>
	      <th>Genero</th>
	      <th>actualizar</th>
	      <th>eliminar</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($maestros as $maestro)
	    <tr>
	      
	      <td>{{$maestro->nombre}} {{$maestro->apellido }}</td>
	      <td>{{$maestro->genero}} </td>
	     
	      <td><a href=" {{ route('teacher.edit', $maestro->id) }} ">Editar</a></td>
	     
	      <td>	
				<form method="POST" action="{{ route('teacher.destroy', $maestro->id) }}">
					@csrf
					{!! method_field('DELETE') !!}
					<button type="submit">Eliminar</button>
				</form>
	      	</td>



	    </tr>
	    @endforeach	
	  </tbody>
</table>

<a href=" {{ route('teacher.create') }} ">Crear Maestro</a>

	
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Colegio</title>
</head>
<body>

	<a href="http://colegio.test/">Inicio</a>
	<table>
	  <thead>
	    <tr>
	      <th>Nombre</th>
	      <th>Genero</th>
	      <th>Fecha Nacimiento</th>
	      <th>actualizar</th>
	      <th>eliminar</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($estudiantes as $est)
	    <tr>
	      
	      <td>{{$est->nombre}} {{$est->apellido }}</td>
	      <td>{{$est->genero}} </td>
	      <td>{{$est->fecha_nacimiento}}</td>
	      <td><a href=" {{ route('student.edit', $est->id) }} ">Editar</a></td>
	     
	      <td>	
				<form method="POST" action="{{ route('student.destroy', $est->id) }}">
					@csrf
					{!! method_field('DELETE') !!}
					<button type="submit">Eliminar</button>
				</form>
	      	</td>



	    </tr>
	    @endforeach	
	  </tbody>
</table>


<a href=" {{ route('teacher.create') }} ">Crear Estudiante</a>
	
</body>
</html>